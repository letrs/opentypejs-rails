# opentypejs-rails

[opentype.js](https://opentype.js.org) library bundled for Rails.

## Usage

Gemfile:

```ruby
gem 'opentypejs-rails', :git => 'https://bitbucket.org/letrs/opentypejs-rails', :tag => '0.0.0'
```

application.js:

```javascript
//= require opentype
```
